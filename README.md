# bash-exit

CI/CD pipeline which exits with failure as soon as a `script` has an error in it

---

I learnt from [a blog post](https://cuddly-octo-palm-tree.com/posts/2021-01-17-bash-set-dash-e/) that you can set bash to exit as soon as an error occurs, using it's `-e` option (exit immediately) either with `bash -e`, or by adding `set -e` to the script. Bash runs everything in the script up to the error, and then stops, returning the last return code, which will be non-zero.

So, I tested what happens in GitLab CI `script` blocks if you put in a non-existant command:  It [fails when the script is evaluated](https://gitlab.com/mlockhart-hax/gitlab-pipelines/bash-exit/-/jobs/1610473837#L21), before it's executed. Any good code before the failure is _not_ executed in [this case](https://gitlab.com/mlockhart-hax/gitlab-pipelines/bash-exit/-/jobs/1610480111).

Note also that the error in this case comes from `/bin/sh`, so bash is run in POSIX mode.

The GitLab documentation mentions that CI will also behave like `bash -e` by default, and that you [can eat the non-zero exit code to disable this behavior](https://docs.gitlab.com/ee/ci/yaml/script.html#ignore-non-zero-exit-codes).

Indeed, so long as the command you try to run _does exist_ ([so that it evaluates](https://gitlab.com/mlockhart-hax/gitlab-pipelines/bash-exit/-/jobs/1610498754#L21)), then it will run, and [stop at the point of failure](https://gitlab.com/mlockhart-hax/gitlab-pipelines/bash-exit/-/jobs/1610502439#L41).
